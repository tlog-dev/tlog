module tlog.app/go/tlog

go 1.16

require (
	github.com/ClickHouse/clickhouse-go/v2 v2.10.1
	github.com/fsnotify/fsnotify v1.6.0
	github.com/gin-gonic/gin v1.9.1
	github.com/google/uuid v1.3.0
	github.com/nikandfor/assert v0.0.0-20231111164855-4827b9a9a365
	github.com/nikandfor/cli v0.0.0-20230428202915-fdf5f3ba6b67
	github.com/nikandfor/errors v0.8.0
	github.com/nikandfor/graceful v0.0.0-20230428203120-c0dcdbcf0a88
	github.com/nikandfor/hacked v0.0.0-20230717052658-a86e97bdda4d
	github.com/nikandfor/loc v0.5.0
	github.com/nikandfor/tlog v0.21.4 // indirect
	github.com/stretchr/testify v1.8.4
	golang.org/x/exp v0.0.0-20230522175609-2e198f4a06a1
	golang.org/x/term v0.9.0
	tlog.app/go/eazy v0.0.0-20230717073616-6a466ff05869
)
